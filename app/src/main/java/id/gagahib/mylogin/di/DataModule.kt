package id.gagahib.mylogin.di

import dagger.Binds
import dagger.Module
import id.gagahib.mylogin.data.DataRepository
import id.gagahib.mylogin.data.DataRepositorySource
import javax.inject.Singleton

// Tells Dagger this is a Dagger module
@Module
abstract class DataModule {
    @Binds
    @Singleton
    abstract fun provideDataRepository(dataRepository: DataRepository): DataRepositorySource
}
