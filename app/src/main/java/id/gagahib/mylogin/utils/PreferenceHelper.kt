package id.gagahib.mylogin.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

object PreferenceHelper {

    val PREFERENCE_NAME = "MYLOGIN-PREFS"
    val EMAIL_ID = "EMAIL"
    val PASSWORD = "PASSWORD"
    val AUTO_LOGIN = "AUTO_LOGIN"

    fun customPreference(context: Context): SharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val edit = edit()
        operation(edit)
        edit.apply()
    }

    var SharedPreferences.emailId
        get() = getString(EMAIL_ID, "")
        set(value) {
            edit {
                it.putString(EMAIL_ID, value)
            }
        }

    var SharedPreferences.password
        get() = getString(PASSWORD, "")
        set(value) {
            edit {
                it.putString(PASSWORD, value)
            }
        }

    var SharedPreferences.autoLogin
        get() = getBoolean(AUTO_LOGIN, false)
        set(value) {
            edit {
                it.putBoolean(AUTO_LOGIN, value)
            }
        }

    var SharedPreferences.clearValues
        get() = { }
        set(value) {
            edit {
                it.clear()
                Log.d("ClearPref",  "Result : "+getString(EMAIL_ID, "kosong"))
            }
        }
}