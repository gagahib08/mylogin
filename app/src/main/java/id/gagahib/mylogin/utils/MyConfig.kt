package id.gagahib.mylogin.utils

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.View

object MyConfig{

    var density: Float = 0.0f

    fun DensityUtil() {
        density = Resources.getSystem().getDisplayMetrics().density;
    }

    fun dp2px(dpValue: Float): Int {
        return (0.5f + dpValue * Resources.getSystem()
            .displayMetrics.density).toInt()
    }

    fun px2dp(pxValue: Float): Float {
        return pxValue / Resources.getSystem().displayMetrics.density
    }

    fun dip2px(dpValue: Float): Int {
        return (0.5f + dpValue * density).toInt()
    }

    fun px2dip(pxValue: Float): Float {
        return pxValue / density
    }
}