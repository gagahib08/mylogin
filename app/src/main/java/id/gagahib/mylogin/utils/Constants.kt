package id.gagahib.mylogin.utils

class Constants {
    companion object INSTANCE {
        const val SPLASH_DELAY = 3000
        const val BASE_URL_ = "https://gagahib.id/default/jaktur/"
        const val FCM_TOKEN = "fcm_token"
        const val USER_AGENT = "Android"
        const val USER_DATA = "user_data"
    }
}
