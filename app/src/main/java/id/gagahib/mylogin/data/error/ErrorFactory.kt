package id.gagahib.mylogin.data.error


interface ErrorFactory {
    fun getError(errorCode: Int): Error
}