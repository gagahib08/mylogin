package id.gagahib.mylogin.data.remote

import id.gagahib.mylogin.MyLoginApplication
import id.gagahib.mylogin.data.Resource
import id.gagahib.mylogin.data.error.Error.Companion.NETWORK_ERROR
import id.gagahib.mylogin.data.error.Error.Companion.NO_INTERNET_CONNECTION
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import id.gagahib.mylogin.data.remote.service.ApiService
import id.gagahib.mylogin.utils.Network.Utils.isConnected
import id.gagahib.mylogin.utils.wrapEspressoIdlingResource
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class RemoteData @Inject
constructor(private val serviceGenerator: ServiceGenerator) : RemoteDataSource {

    private suspend fun processCall(responseCall: suspend () -> Response<*>): Any? {
        if (!isConnected(MyLoginApplication.context)) {
            return NO_INTERNET_CONNECTION
        }
        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                responseCode
            }
        } catch (e: IOException) {
            NETWORK_ERROR
        }
    }

    override suspend fun doLogin(loginPayload: LoginPayload): Resource<LoginResponse> {
        wrapEspressoIdlingResource {
            val apiService = serviceGenerator.createService(ApiService::class.java)
            return when (val response = processCall { apiService.doLogin(loginPayload) }) {
                is LoginResponse -> {
                    Resource.Success(data = response)
                }
                else -> {
                    Resource.DataError(errorCode = response as Int)
                }
            }
        }
    }

}
