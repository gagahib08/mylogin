package id.gagahib.mylogin.data.remote.service

import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("check_user")
    suspend fun doLogin(@Body loginPayload: LoginPayload): Response<LoginResponse>
//
//    @POST("api/school-admin/user/login")
//    suspend fun doLogin(): Response<LoginResponse>
}
