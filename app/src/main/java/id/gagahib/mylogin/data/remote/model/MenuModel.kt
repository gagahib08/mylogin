package id.gagahib.mylogin.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class MenuModel(
        @Json(name = "menu_id")
    val menuId: Int,
        @Json(name = "menu_name")
    val menuName: String = "",
        @Json(name = "menu_category_id")
    val menuCategoryId: String = ""
) : Serializable