package id.gagahib.mylogin.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ObjectResponse(
        @Json(name = "data")
    val data: DataResponse
)