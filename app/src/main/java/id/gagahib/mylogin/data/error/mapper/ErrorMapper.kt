package id.gagahib.mylogin.data.error.mapper

import id.gagahib.mylogin.data.error.Error
import id.gagahib.mylogin.R
import id.gagahib.mylogin.MyLoginApplication
import javax.inject.Inject

class ErrorMapper @Inject constructor() : ErrorMapperInterface {

    override fun getErrorString(errorId: Int): String {
        return MyLoginApplication.context.getString(errorId)
    }

    override val errorsMap: Map<Int, String>
        get() = mapOf(
                Pair(Error.NO_INTERNET_CONNECTION, getErrorString(R.string.no_internet)),
                Pair(Error.NETWORK_ERROR, getErrorString(R.string.network_error))
        ).withDefault { getErrorString(R.string.something_error) }
}