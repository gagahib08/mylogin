package id.gagahib.mylogin.data.remote

import id.gagahib.mylogin.data.Resource
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload

internal interface RemoteDataSource {
    suspend fun doLogin(loginPayload: LoginPayload): Resource<LoginResponse>
}
