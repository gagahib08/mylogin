package id.gagahib.mylogin.data.remote.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginResponse(
        @Json(name = "version")
    val version: String = "",
        @Json(name = "status")
    val status: String = "",
        @Json(name = "data")
    val data: DataResponse,
        @Json(name = "message")
    val message: String = ""
)