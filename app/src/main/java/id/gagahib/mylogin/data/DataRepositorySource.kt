package id.gagahib.mylogin.data

import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import kotlinx.coroutines.flow.Flow

interface DataRepositorySource {
//    suspend fun fetchProduct(): Flow<Resource<List<DataResponse>>>

    suspend fun doLogin(loginPayload: LoginPayload): Flow<Resource<LoginResponse>>
}
