package id.gagahib.mylogin.data.remote.model


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize


@JsonClass(generateAdapter = true)
@Parcelize
data class Category(
        @Json(name = "imageUrl")
        val imageUrl: String = "",
        @Json(name = "id")
        val id: Int,
        @Json(name = "name")
        val name: String = ""
) : Parcelable