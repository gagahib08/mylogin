package id.gagahib.mylogin.data.remote.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class DataResponse(
    @Json(name = "user_id")
    val userId: String = "",
    @Json(name = "user_image")
    val userImage: String = "",
    @Json(name = "user_phone")
    val userPhone: String = "",
    @Json(name = "user_name")
    val userName: String = "",
    @Json(name = "email")
    val email: String = "",
    @Json(name = "instagram")
    val instagram: String = "",
    @Json(name = "linkedin")
    val linkedin: String = "",
    @Json(name = "jobs")
    val jobs: String = "",
    @Json(name = "website")
    val website: String = "",
    @Json(name = "address")
    val address: String = "",
    @Json(name = "gender")
    val gender: String = "",
    @Json(name = "place_birth")
    val placeBirth: String = ""
): Serializable