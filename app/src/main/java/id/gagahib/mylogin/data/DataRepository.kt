package id.gagahib.mylogin.data

import id.gagahib.mylogin.data.local.LocalData
import id.gagahib.mylogin.data.remote.RemoteData
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class DataRepository @Inject
constructor(private val remoteRepository: RemoteData, private val localRepository: LocalData) : DataRepositorySource {

    override suspend fun doLogin(loginPayload: LoginPayload): Flow<Resource<LoginResponse>> {
        return flow {
//                emit(Resource.Loading())
                emit(remoteRepository.doLogin(loginPayload))
            }.flowOn(Dispatchers.IO)
    }
}
