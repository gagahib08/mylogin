package id.gagahib.mylogin.data.remote.payload

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginPayload(
    @SerializedName("password")
        val password: String,
    @SerializedName("email")
        val email: String
) {
    companion object {
        fun generate(
            password: String,
            phone: String
        ): LoginPayload {
            return LoginPayload(
                password = password,
                email = phone
            )
        }
    }
}