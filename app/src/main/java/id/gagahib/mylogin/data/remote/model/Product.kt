package id.gagahib.mylogin.data.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Product(
        @Json(name = "id")
    val id: String = "",
        @Json(name = "imageUrl")
    val imageUrl: String = "",
        @Json(name = "title")
    val title: String = "",
        @Json(name = "description")
    val description: String = "",
        @Json(name = "price")
    val price: String = "",
        @Json(name = "loved")
    val loved: Int
)