package id.gagahib.mylogin.ui.component.main

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import com.squareup.picasso.Picasso
import id.gagahib.mylogin.R
import id.gagahib.mylogin.data.remote.model.DataResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import id.gagahib.mylogin.databinding.ActivityMainBinding
import id.gagahib.mylogin.ui.ViewModelFactory
import id.gagahib.mylogin.ui.base.BaseActivity
import id.gagahib.mylogin.ui.component.login.LoginViewModel
import id.gagahib.mylogin.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity: BaseActivity(), View.OnClickListener{

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var mainViewModel: MainViewModel

    private lateinit var binding: ActivityMainBinding

    private var backPressedTime: Long = 0
    lateinit var backToast: Toast

    private var mOffset = 0
    private var mScrollY = 0

    override fun initViewBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        mainViewModel = viewModelFactory.create(MainViewModel::class.java)
    }

    override fun observeViewModel() {
        observe(mainViewModel.userData, ::initializeView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel.userData.value = intent.getSerializableExtra(Constants.USER_DATA) as DataResponse?

        StatusBarUtil.darkMode(this)
        StatusBarUtil.setPaddingSmart(this, binding.toolbar)
        initViewListener()
    }

    private fun initViewListener(){
        binding.scrollView.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            private var lastScrollY = 0
            private val h = MyConfig.dp2px(170f)
            private val color = ContextCompat.getColor(applicationContext, R.color.colorWhite) and 0x00ffffff
            override fun onScrollChange(v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
                var tScrollY= scrollY
                if (lastScrollY < h) {
                    tScrollY = Math.min(h, tScrollY)
                    mScrollY = if (tScrollY > h) h else tScrollY
                    buttonBarLayout.alpha = 1f * mScrollY / h
                    toolbar.setBackgroundColor(255 * mScrollY / h shl 24 or color)
                    parallax.translationY = (mOffset - mScrollY).toFloat()
                }
                lastScrollY = tScrollY
            }
        })
        buttonBarLayout.alpha = 0f
        toolbar.setBackgroundColor(0)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onClick(v: View?) {
        when {

        }
    }

    override fun onBackPressed() {
        if (doubleBackPressed()){
            super.onBackPressed()
        }
    }

    fun doubleBackPressed() :Boolean {
        backToast = Toast.makeText(this, getString(R.string.press_back_again_to_leave), Toast.LENGTH_SHORT)
        return if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel()
            true
        } else {
            backToast.show()
            backPressedTime = System.currentTimeMillis()
            false
        }
    }

    private fun initializeView(userData: DataResponse) {

        binding.tvAddress.text = userData.email
        binding.tvTitleName.text = userData.userName
        binding.tvUserName.text = userData.userName
        binding.tvJobs.text = userData.jobs
        binding.tvAddress.text = userData.address
        binding.tvGender.text = userData.gender
        binding.tvPlaceBirth.text = userData.placeBirth
        binding.tvEmail.text = userData.email
        binding.tvPhone.text = userData.userPhone

        binding.linWebsite.setOnClickListener { openSites(userData.website) }
        binding.linInstagram.setOnClickListener { openSites(userData.instagram) }
        binding.linLinkedIn.setOnClickListener { openSites(userData.linkedin) }
        binding.linEmail.setOnClickListener { openMail(userData.email, userData.userName) }
        binding.linPhone.setOnClickListener { actionDial(userData.userPhone) }


        Picasso
            .get()
            .load(userData.userImage)
            .placeholder(R.color.colorGray)
            .into(binding.ivTitleImage)
        Picasso
            .get()
            .load(userData.userImage)
            .placeholder(R.color.colorGray)
            .into(binding.ivUserImage)

    }

    fun openSites(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    fun openMail(email: String, name: String){
        val intent = Intent(Intent.ACTION_SEND)
        intent.data = Uri.parse("mailto:")
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Hi "+name)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    fun actionDial(phone: String) {
        val dialIntent = Intent(Intent.ACTION_DIAL)
        dialIntent.data = Uri.parse("tel:" + phone)
        startActivity(dialIntent)
    }
}