package id.gagahib.mylogin.ui.component.main

import android.content.Intent
import android.net.Uri
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.gagahib.mylogin.data.DataRepositorySource
import id.gagahib.mylogin.data.Resource
import id.gagahib.mylogin.data.error.ErrorManager
import id.gagahib.mylogin.data.error.mapper.ErrorMapper
import id.gagahib.mylogin.data.remote.model.DataResponse
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import id.gagahib.mylogin.ui.base.BaseViewModel
import id.gagahib.mylogin.utils.Event
import id.gagahib.mylogin.utils.wrapEspressoIdlingResource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject
constructor(private val dataRepositoryRepository: DataRepositorySource) : BaseViewModel() {

    override val errorManager
    get() = ErrorManager(ErrorMapper())

    var userData: MutableLiveData<DataResponse> = MutableLiveData()



}
