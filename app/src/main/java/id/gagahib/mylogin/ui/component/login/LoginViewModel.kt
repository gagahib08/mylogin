package id.gagahib.mylogin.ui.component.login

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.gagahib.mylogin.data.DataRepositorySource
import id.gagahib.mylogin.data.Resource
import id.gagahib.mylogin.data.error.ErrorManager
import id.gagahib.mylogin.data.error.mapper.ErrorMapper
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import id.gagahib.mylogin.ui.base.BaseViewModel
import id.gagahib.mylogin.utils.Event
import id.gagahib.mylogin.utils.wrapEspressoIdlingResource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject
constructor(private val dataRepositoryRepository: DataRepositorySource) : BaseViewModel() {

    override val errorManager
    get() = ErrorManager(ErrorMapper())

    @VisibleForTesting
    val _loginLiveData = MutableLiveData<Resource<LoginResponse>>()
    val loginLiveData: LiveData<Resource<LoginResponse>> get() = _loginLiveData

    /**
     * Error handling as UI
     */
    private val showSnackBarPrivate = MutableLiveData<String>()
    val showSnackBar: LiveData<String> get() = showSnackBarPrivate

    private val showToastPrivate = MutableLiveData<Event<Any>>()
    val showToast: LiveData<Event<Any>> get() = showToastPrivate

    fun doLogin(loginPayload: LoginPayload) {
        viewModelScope.launch {
            _loginLiveData.value = Resource.Loading()
            wrapEspressoIdlingResource {
                dataRepositoryRepository.doLogin(loginPayload).collect {
                    _loginLiveData.value = it
                }
            }
        }
    }


    fun showSnackBarMessage(message: String) {
        showSnackBarPrivate.value = message
    }

    fun showToastMessage(errorCode: Int) {
        val error = errorManager.getError(errorCode)
        showToastPrivate.value = Event(error.description)
    }



}
