package id.gagahib.mylogin.ui.base.listeners

import id.gagahib.mylogin.data.remote.model.Product

interface RecyclerItemListener {
    fun onItemSelected(product: Product)
}
