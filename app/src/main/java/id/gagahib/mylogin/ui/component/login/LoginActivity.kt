package id.gagahib.mylogin.ui.component.login

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.widget.RxTextView
import id.gagahib.mylogin.ui.component.main.MainActivity
import id.gagahib.mylogin.R
import id.gagahib.mylogin.data.Resource
import id.gagahib.mylogin.data.remote.model.DataResponse
import id.gagahib.mylogin.data.remote.model.LoginResponse
import id.gagahib.mylogin.data.remote.payload.LoginPayload
import id.gagahib.mylogin.databinding.ActivityLoginBinding
import id.gagahib.mylogin.ui.ViewModelFactory
import id.gagahib.mylogin.ui.base.BaseActivity
import id.gagahib.mylogin.utils.*
import id.gagahib.mylogin.utils.PreferenceHelper.autoLogin
import id.gagahib.mylogin.utils.PreferenceHelper.clearValues
import id.gagahib.mylogin.utils.PreferenceHelper.emailId
import id.gagahib.mylogin.utils.PreferenceHelper.password
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class LoginActivity: BaseActivity(), View.OnClickListener{
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var loginViewModel: LoginViewModel

    var isShowPassword : Boolean = false

    private lateinit var binding: ActivityLoginBinding

    override fun initViewBinding() {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    override fun initializeViewModel() {
        loginViewModel = viewModelFactory.create(loginViewModel::class.java)
    }

    override fun observeViewModel() {
        observe(loginViewModel.loginLiveData, ::handleLogin)
        observeToast(loginViewModel.showToast)
        observeSnackBarMessages(loginViewModel.showSnackBar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        StatusBarUtil.darkMode(this)
        StatusBarUtil.setPaddingSmart(this, binding.relBinding)

        initGetInfo()
        initViewListener()
        initInputChecker()
    }

    private fun initViewListener(){

        binding.buSignIn.setOnClickListener(this)
        binding.ivShowPassword.setOnClickListener(this)
        binding.linSignInGoogle.setOnClickListener(this)
        binding.linSignInFacebook.setOnClickListener(this)
        binding.linNewUser.setOnClickListener(this)
        binding.tvForgotPassword.setOnClickListener(this)

    }

    private fun initInputChecker(){
        RxTextView.afterTextChangeEvents(binding.etEmail)
            .skipInitialValue()
            .map {
                binding.etEmail.error = null
                it.view().text.toString()
            }
            .debounce(1500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .compose(validateEmailAddress)
            .compose(retryWhenError {
                binding.etEmail.error = it.message
            })
            .subscribe()
    }

    private inline fun retryWhenError(crossinline onError: (ex: Throwable) -> Unit): ObservableTransformer<String, String> = ObservableTransformer { observable ->
        observable.retryWhen { errors ->
            errors.flatMap {
                onError(it)
                Observable.just("")
            }
        }
    }

    private val validateEmailAddress = ObservableTransformer<String, String> { observable ->
        observable.flatMap {
            Observable.just(it).map { it.trim() }
                .filter {
                    Patterns.EMAIL_ADDRESS.matcher(it).matches()
                }
                .singleOrError()
                .onErrorResumeNext {
                    if (it is NoSuchElementException) {
                        Single.error(Exception(getString(R.string.please_enter_a_valid_email_address)))
                    } else {
                        Single.error(it)
                    }
                }
                .toObservable()
        }
    }

    override fun onClick(v: View?) {
        when {
            v?.id == R.id.buSignIn -> {
                if(checkEmpty()){
                    loginViewModel.doLogin(LoginPayload.generate(etPassword.editableText.toString(), etEmail.editableText.toString()))
                }
                binding.relBinding.hideKeyboard()
            }
            v?.id == R.id.ivShowPassword -> {
                binding.ivShowPassword.hideShowPassword(isShowPassword)
                binding.etPassword.hideShowPassword(isShowPassword)
                isShowPassword = !isShowPassword
            }
            v?.id == R.id.linSignInGoogle -> {
                
            }
            v?.id == R.id.linSignInFacebook -> {

            }
            v?.id == R.id.linNewUser -> {

            }
            v?.id == R.id.tvForgotPassword -> {

            }
        }
    }

    private fun initGetInfo(){
        val prefs = PreferenceHelper.customPreference(this)
        if (prefs.autoLogin){
            binding.etEmail.setText(prefs.emailId.toString())
            binding.etPassword.setText(prefs.password.toString())
            binding.cbRememberMe.isChecked = prefs.autoLogin
        }
    }

    private fun initSaveInfo(){
        val prefs = PreferenceHelper.customPreference(this)
        if (binding.cbRememberMe.isChecked){
            prefs.autoLogin = true
            prefs.emailId = binding.etEmail.text.toString()
            prefs.password = binding.etPassword.text.toString()
        }else{
            prefs.clearValues = {}
        }
    }

    private fun handleLogin(loginResponse: Resource<LoginResponse>){
        when (loginResponse) {
            is Resource.Loading -> showLoadingView()
            is Resource.Success -> loginResponse.data?.let { bindLoginResponse(loginResponse =  it) }
            is Resource.DataError -> {
                loginResponse.errorCode?.let {
                    hideLoadingView()
                    loginViewModel.showToastMessage(it)
                }
            }
        }
    }

    private fun bindLoginResponse(loginResponse: LoginResponse) {
        if (loginResponse.status.equals("success")){
            initSaveInfo()
            navigateToDashboardScreen(loginResponse.data)
        } else{
            var errMsg = loginResponse.message

            if (errMsg.contains("email", true)){
                binding.etEmail.showError(errMsg)
            }else if (errMsg.contains("password", true)){
                binding.etPassword.showError(errMsg)
            }
            loginViewModel.showSnackBarMessage(errMsg)
        }
        hideLoadingView()
    }

    private fun navigateToDashboardScreen(user: DataResponse) {
        val mainScreenIntent = Intent(this, MainActivity::class.java).apply {
            putExtra(Constants.USER_DATA, user)
        }
        startActivity(mainScreenIntent)
        finish()
    }

    private fun checkEmpty(): Boolean{
        var checker: Int = 1;

        if (!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.editableText.toString()).matches()){
            binding.etEmail.error = getString(R.string.please_enter_a_valid_email_address)
            checker *= 0
        }
        if (binding.etPassword.length() == 0){
            binding.etPassword.error = getString(R.string.please_enter_a_valid_password)
            checker *= 0
        }
        return checker!=0

    }

    private fun observeSnackBarMessages(event: LiveData<String>) {
        binding.relBinding.setupSnackbar(this, event, Snackbar.LENGTH_LONG)
    }

    private fun observeToast(event: LiveData<Event<Any>>) {
        binding.relBinding.showToast(this, event, Snackbar.LENGTH_LONG)
    }

    private fun showLoadingView() {
        binding.pbLoading.toVisible()
    }

    private fun hideLoadingView(){
        binding.pbLoading.toGone()
    }
}