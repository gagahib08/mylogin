package id.gagahib.mylogin.ui.base

import androidx.lifecycle.ViewModel
import id.gagahib.mylogin.data.error.ErrorManager

abstract class BaseViewModel : ViewModel() {
    /**Inject Singleton ErrorManager
     * Use this errorManager to get the Errors
     */
    abstract val errorManager: ErrorManager

}
